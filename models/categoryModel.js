

const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    name: String,
    description: String,
    image: String, 
    createdAt: Date
});

module.exports = CategoryModel = mongoose.model('CategoryGM', categorySchema);
