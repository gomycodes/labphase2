

const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: String,
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CategoryGM',
        required: false
    },
    description: String,
    image: String, 
    price: Number,
    createdAt: Date
});

module.exports = ProductModel = mongoose.model('ProductGM', productSchema);
