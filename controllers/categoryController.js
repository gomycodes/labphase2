const categoryService = require("../services/categoryService")

exports.create = async (req, res) =>{

  const data = await categoryService.create(req, res);
  return res.status(201).json(data);
}

exports.all = async (req, res) =>{

  const data = await categoryService.all(req, res);
  return res.status(200).json(data);
}

exports.find = async (req, res) =>{

  const data = await categoryService.find(req, res);
  return res.status(200).json(data);  
}

exports.update = async (req, res) =>{
  
  const data = await categoryService.update(req, res);
  return res.status(200).json(data);
}

exports.destroy = (req, res) =>{
    
  const data = categoryService.destroy(req, res);
  return res.status(200).json(data);
}