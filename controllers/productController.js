const productService = require("../services/productService")

exports.create = async (req, res) =>{

  const data = await productService.create(req, res);
  return res.status(201).json(data);
}

exports.all = async (req, res) =>{

  const data = await productService.all(req, res);
  return res.status(200).json(data);
}

exports.find = async (req, res) =>{

  const data = await productService.find(req, res);
  return res.status(200).json(data);  
}

exports.update = async (req, res) =>{
  
  const data = await productService.update(req, res);
  return res.status(200).json(data);
}

exports.destroy = async (req, res) =>{
    
  const data = await productService.destroy(req, res);
  return res.status(200).json(data);
}