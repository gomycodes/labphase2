const express = require('express')
const app = express()

const cors = require('cors');
app.use(cors());
//CORS bypass
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    next();
});

const productRouter = require('./routes/productRouter');
const categoryRouter = require('./routes/categoryRouter');
require('dotenv').config()

const bodyParser = require('body-parser');
const mongoose = require('mongoose');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect("mongodb+srv://root:root2022@cluster0.2eimy.mongodb.net/?retryWrites=true&w=majority",()=>{
    console.log("mongodb started")
});

app.use('/products', productRouter);
app.use('/categories', categoryRouter);

app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`)
})
  