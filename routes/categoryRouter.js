const router = require('express').Router();
const categoryController = require('../controllers/categoryController');

router.post('/', categoryController.create)
router.get('/', categoryController.all)
router.get('/:id', categoryController.find)
router.put('/:id', categoryController.update)
router.delete('/:id', categoryController.destroy)

module.exports = router;