const router = require('express').Router();
const productController = require('../controllers/productController');

router.post('/', productController.create)
router.get('/', productController.all)
router.get('/:id', productController.find)
router.put('/:id', productController.update)
router.delete('/:id', productController.destroy)

module.exports = router;