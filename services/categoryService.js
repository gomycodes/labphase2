const categoryModel = require("../models/categoryModel")

exports.create = async (req, res) =>{

    const data = new categoryModel({
        name: req.body.name,
        description: req.body.description,
        image: req.body.image,
        createdAt: Date.now()
    })

  product = await data.save();

  return req.body;
}

exports.all = async(req, res) =>{
 const data = await categoryModel.find();
 return data;
}

exports.find = async (req, res) =>{

const product = await categoryModel.findById(req.params.id);
if(product){
    return product;
}
return {message: "Data not found"};
}

exports.update = async (req, res) =>{

    console.log(req.params.id, req.body)
    const product = await categoryModel.findByIdAndUpdate(req.params.id, req.body)
    if(product){
    return res.status(200).json(product);
    }
    res.status(404).json({message: 'Data not found', data: product});
}

exports.destroy = async (req, res) =>{
    
    const data = await categoryModel.findByIdAndDelete(req.params.id);
    if(data) return {message: "successful"};
    return {message: 'Data not found'};
}
