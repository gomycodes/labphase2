const ProductModel = require("../models/productModel")

exports.create = async (req, res) =>{

    const data = new ProductModel({
        name: req.body.name,
        description: req.body.description,
        image: req.body.image,
        category: req.body.category,
        price: parseFloat(req.body.price),
        createdAt: Date.now()
    })

  product = await data.save();

  return product;
}

exports.all = async(req, res) =>{
 const data = await ProductModel.find();
 return data;
}

exports.find = async (req, res) =>{

const product = await ProductModel.findById(req.params.id);
if(product){
    return product;
}
return {message: "Data not found"};
}

exports.update = async (req, res) =>{

    const product = await ProductModel.findByIdAndUpdate(req.params.id, req.body)
    if(product){
    return res.status(200).json(product);
    }
    res.status(404).json({message: 'Data not found'});
}

exports.destroy = async (req, res) =>{
    
    const data = await ProductModel.findByIdAndDelete(req.params.id);
    if(data) return {message: "successful"};
    return {message: 'Data not found'};
}
